# MO - T&R

## Description
This repository stores Monero Outreach translations and reviews.

## Contributing
Before you submit any work to this repository please read the [Taiga instance](https://taiga.getmonero.org/project/xmrhaelan-monero-public-relations/epic/330).

### Fonts
- [Monero Typography](https://www.monerooutreach.org/monero-typography.php). _Free use._

## Authors and acknowledgment
We are a team of creators, writers, technicians, translators, and visionaries. Monero Outreach began in June 2018 and has continued to gain new, talented contributors. We are a workgroup of the decentralized Monero community, and as a result we do not claim any level of authority over Monero - we are simply here to follow the mandate that we have set for ourselves, because we love Monero.

## Resources
- Web: [monerooutreach.org](https://www.monerooutreach.org/)
- Mail:
   - [monerooutreach@protonmail.com](mailto:monerooutreach@protonmail.com)
   - [paintluis@protonmail.com](mailto:paintluis@protonmail.com)
- Gitlab: [MO Translations and Reviews](https://gitlab.com/monerooutreach/mo-translations-reviews)
- Github: [outreach-docs](https://github.com/monero-ecosystem/outreach-docs)
- Telegram:
  - [@monerooutreach join link](https://t.me/monerooutreach)
  - [@outreachtranslations join link](https://t.me/outreachtranslations)

- For support and coordination
  - [`#monero-outreach:matrix.org`](https://matrix.to/#/#monero-outreach:monero.social?via=matrix.org&via=monero.social&via=haveno.network)
  - [`#monero-outreach:monero.social`](https://matrix.to/#/#monero-outreach:monero.social?via=matrix.org&via=monero.social&via=haveno.network)(Matrix/Riot)
- Monero Outreach [Taiga instance](https://taiga.getmonero.org/project/xmrhaelan-monero-public-relations/).
- Maintainers:
   - [lh1008](https://gitlab.com/lh1008) - Monero Outreach Coordinator
   - [xmrhaelan](https://github.com/xmrhaelan) - Monero Outreach Organizer

## License
This work is licensed under Creative Commons [Attribution-NonCommercial 4.0 International](https://creativecommons.org/licenses/by-nc/4.0/) - `cc-by-nc-4.0`

## Support Monero Outreach
The Monero Outreach team is continuing to develop innovative ways to make more people aware of Monero and the communities efforts, we are building a very talented team of contributors to help make it happen. Please consider supporting this effort.

Monero Outreach XMR Address:
`47oKHkoaQdBdFpTJNKaetUS6UsCGHVbJbGxPGaaHFQPqXSCLbqXYsBo6x7abwtfdXTeiBhtZLnYF5bRRAhYsUVb5Sd1aqiD`
